(function($) {
  $(function() {

    /* ---------------------------------------------------------*/
    /* VARIABLES */
    /* ---------------------------------------------------------*/
    // set a delay between keystrokes that will trigger a search
    var delay = Drupal.settings.apachesolr_instant['input_delay'];
    // @todo: use a drupal standard selector and allow user to specify their own.
    var content_container = $('.search-results-wrapper');
    // insert the instant results wrapper
    content_container.before('<div id="instant-results"></div>');
    var result_container = $('#instant-results');
    var search_input = $('#apachesolr_instant_search');
    var base_path = Drupal.settings.basePath;
    var min_keyword_length = Drupal.settings.apachesolr_instant['min_keyword_length'];

    /* ---------------------------------------------------------*/
    /* DISABLE SUBMIT */
    /* ---------------------------------------------------------*/
    search_input.submit(function(e) {
      document.activeElement.blur();
      e.preventDefault();
      return false;
    });

    /* ---------------------------------------------------------*/
    /* LOAD MORE */
    /* ---------------------------------------------------------*/
    $('#apachesolr-live-load-more').live('click', function(e) {
      console.log('load more clicked');
      var more = $(this);
      var start = more.attr('data-page');
      var keys = more.attr('data-keys');
      var facetkey = more.attr('data-facetkey');
      var facetvalue = more.attr('data-facetvalue');
      var facetlink = {0:{'key':facetkey, 'value':facetvalue}};
      var query = {
        'keys':keys,
        'query':facetlink,
        'page':start
      };
      query = JSON.stringify(query);
      apachesolr_instant_search(query, true);
//      more.remove();
//      var anchor = more.attr('href');
//      scrollToAnchor(anchor);
      e.preventDefault();
      return false;
    });

    /* ---------------------------------------------------------*/
    /* SEARCH INPUT DETECTED */
    /* ---------------------------------------------------------*/
    // @todo: turn this into a function and call from a Drupal.behaviours implementation
    // perform search and replace results
    search_input.stop(true, true).bindWithDelay('keyup', function() {
      var last_keywords = Drupal.settings.apachesolr_instant['keyword_placeholder'];
      // current search input
      var keys = $(this).val();
      // make sure more than minimum characters have been entered
      if ($.trim(keys).length >= min_keyword_length) {
        // if no alphanumerical characters have been entered no need to run a query
        if ($.trim(last_keywords) != $.trim(keys)) {
          // Encode the query
          var keywords = JSON.stringify({'keys': keys});
          // run a search and store the results
          apachesolr_instant_search(keywords);
        }
      }
      else {
        apachesolr_instant_hide();
      }
      // set the keyword placeholder to the keyword for later comparison
      Drupal.settings.apachesolr_instant['keyword_placeholder'] = keys;
      return false;
    }, delay);

    /**
     * Hide the apachesolr instant results and display the original page content
     */
    function apachesolr_instant_hide() {
      // Show the existing page content
      content_container.fadeIn();
      // delete all search results
      result_container.html('');
    }

    /**
     * Function to run the search callback with the given keywords
     * The AJAX call returns the formatted live results
     *
     * @param keywords
     */
    function apachesolr_instant_search(keywords, append) {
      jQuery.ajax({
        type: 'POST',
        url: base_path + 'apachesolr_instant/query/' + escape(keywords),
        success: function(result) {
          var obj = $.parseJSON(result);
          if (obj.output != '') {
            if (append) {
              console.log(obj);
              $('#apachesolr-live-load-more').remove();
              result_container.append(obj.output);
            }
            else {
              // Inject the results
              result_container.html(obj.output);
              // Hide the existing page content
              content_container.hide();
            }
            // call attachBehaviours incase any js needs to be fired again.
            Drupal.attachBehaviors(result_container);
          } else {
            apachesolr_instant_hide();
          }
        }
      });
    }

  });
})(jQuery);