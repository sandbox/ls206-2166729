<?php
/**
 * Include file for apachesolr_instant.
 * Used for administration forms.
 */

/**
 * Administration form for apachesolr_instant.
 */
function apachesolr_instant_settings_form($form, &$form_state) {
  $form = array();
  $form['apachesolr_instant_use_cache'] = array(
    '#title' => t('Use Drupal database cache?'),
    '#description' => t('Use the Drupal cache tables to store search results and serve cached results when possible'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('apachesolr_instant_use_cache', 0),
  );
  $form['apachesolr_instant_min_keyword_length'] = array(
    '#title' => t('Minimum keyword length'),
    '#description' => t('The minimum number of characters entered in the search box before a search is fired.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('apachesolr_instant_min_keyword_length', 3),
  );
  $form['apachesolr_instant_input_delay'] = array(
    '#title' => t('Input delay threshold'),
    '#description' => t('The delay (in miliseconds) between keystrokes before a search is fired.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('apachesolr_instant_input_delay', 250),
  );
  return system_settings_form($form);
}